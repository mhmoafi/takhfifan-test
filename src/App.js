// @flow

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import MainLandingPage from './mainLandingPage';

type propsType = {};

type stateType = {};

class App extends Component<propsType, stateType> {
	constructor(props: propsType) {
		super(props);
		this.state = {};
	}

	componentDidMount() {}

	render() {
		return (
			<div className="App_Body">
				<MainLandingPage />
			</div>
		);
	}
}

export default App;

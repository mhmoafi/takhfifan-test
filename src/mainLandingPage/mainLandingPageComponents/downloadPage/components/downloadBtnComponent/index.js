// @flow
import React from 'react';
import './downloadBtnComponent.css';

type propsType = {
	btnTitle: string,
	onBtnClick: Function,
	isExpand: boolean,
	btnOptionLogo1: string,
	btnOptionLogo2: string,
	btnOptionText1: string,
	btnOptionText2: string,
	isMobile: boolean
};

export default (props: propsType) => {
	if (!props.isMobile) {
		if (props.isExpand) {
			return (
				<div className="downloadBtnComponent_mainContainer">
					<div className="downloadBtnComponent_btnContainer" onClick={props.onBtnClick}>
						<p className="downloadBtnComponent_pBtnText">{props.btnTitle}</p>
						<div className="downloadBtnComponent_arrow" />
					</div>
					<div className="downloadBtnComponent_btnOptionsContainerExtend">
						<div className="downloadBtnComponent_btnOptionsRowContainer">
							<img
								className="downloadBtnComponent_btnOptionsImageStyle"
								src={props.btnOptionLogo1}
								alt="Takhfifn"
							/>
							<p className="downloadBtnComponent_pBtnOptionsText">{props.btnOptionText1}</p>
						</div>
						<div className="downloadBtnComponent_seperatorLine" />
						<div className="downloadBtnComponent_btnOptionsRowContainer">
							<img
								className="downloadBtnComponent_btnOptionsImageStyle"
								src={props.btnOptionLogo2}
								alt="Takhfifn"
							/>
							<p className="downloadBtnComponent_pBtnOptionsText">{props.btnOptionText2}</p>
						</div>
					</div>
				</div>
			);
		} else {
			return (
				<div className="downloadBtnComponent_mainContainer">
					<div className="downloadBtnComponent_btnContainer" onClick={props.onBtnClick}>
						<p className="downloadBtnComponent_pBtnText">{props.btnTitle}</p>
						<div className="downloadBtnComponent_arrow" />
					</div>
				</div>
			);
		}
	} else {
		if (props.isExpand) {
			return (
				<div className="downloadBtnComponent_mainContainer_MobileView">
					<div className="downloadBtnComponent_btnContainer_MobileView" onClick={props.onBtnClick}>
						<p className="downloadBtnComponent_pBtnText_MobileView">{props.btnTitle}</p>
						<div className="downloadBtnComponent_arrow_MobileView" />
					</div>
					<div className="downloadBtnComponent_btnOptionsContainerExtend_MobileView">
						<div className="downloadBtnComponent_btnOptionsRowContainer_MobileView">
							<img
								className="downloadBtnComponent_btnOptionsImageStyle_MobileView"
								src={props.btnOptionLogo1}
								alt="Takhfifn"
							/>
							<p className="downloadBtnComponent_pBtnOptionsText_MobileView">{props.btnOptionText1}</p>
						</div>
						<div className="downloadBtnComponent_seperatorLine_MobileView" />
						<div className="downloadBtnComponent_btnOptionsRowContainer_MobileView">
							<img
								className="downloadBtnComponent_btnOptionsImageStyle_MobileView"
								src={props.btnOptionLogo2}
								alt="Takhfifn"
							/>
							<p className="downloadBtnComponent_pBtnOptionsText_MobileView">{props.btnOptionText2}</p>
						</div>
					</div>
				</div>
			);
		} else {
			return (
				<div className="downloadBtnComponent_mainContainer_MobileView">
					<div className="downloadBtnComponent_btnContainer_MobileView" onClick={props.onBtnClick}>
						<p className="downloadBtnComponent_pBtnText_MobileView">{props.btnTitle}</p>
						<div className="downloadBtnComponent_arrow_MobileView" />
					</div>
				</div>
			);
		}
	}
};

// @flow
import React from 'react';
import './downloadPage.css';
import DownloadBtn from './components/downloadBtnComponent';

type propsType = {
	logoImage: string,
	rightImage: string,
	leftImage: string,
	titleTextRow1: string,
	titleTextRow2: string,
	onAndroidBtnClick: Function,
	onIosBtnClick: Function,
	isAndroidExpand: boolean,
	isIosExpand: boolean,
	appleLogo: string,
	googleLogo: string,
	sibappLogo: string,
	cafebazaarLogo: string,
	appleBtnOptionText: string,
	googleBtnOptionText: string,
	sibappBtnOptionText: string,
	cafebazaarBtnOptionText: string,
	isMobile: boolean
};

export default (props: propsType) => {
	if (!props.isMobile) {
		return (
			<div className="downloadPage_mainContainer">
				<div className="downloadPage_background" />
				<img className="downloadPage_imageRightBackgroundStyle" src={props.rightImage} alt="Takhfifn" />
				<img className="downloadPage_imageLeftBackgroundStyle" src={props.leftImage} alt="Takhfifn" />
				<div className="downloadPage_topContainer">
					<img className="downloadPage_imageLogoStyle" src={props.logoImage} alt="Takhfifn" />
					<p className="downloadPage_pTitleTextRow1">{props.titleTextRow1}</p>
					<p className="downloadPage_pTitleTextRow2">{props.titleTextRow2}</p>
				</div>
				<div className="downloadPage_bottomContainer">
					<div className="downloadPage_downloadBtnsContainer">
						<DownloadBtn
							btnTitle={'Download Android'}
							onBtnClick={props.onAndroidBtnClick}
							isExpand={props.isAndroidExpand}
							btnOptionLogo1={props.googleLogo}
							btnOptionText1={props.googleBtnOptionText}
							btnOptionLogo2={props.cafebazaarLogo}
							btnOptionText2={props.cafebazaarBtnOptionText}
							isMobile={props.isMobile}
						/>
						<DownloadBtn
							btnTitle={'Download iOS'}
							onBtnClick={props.onIosBtnClick}
							isExpand={props.isIosExpand}
							btnOptionLogo1={props.appleLogo}
							btnOptionText1={props.appleBtnOptionText}
							btnOptionLogo2={props.sibappLogo}
							btnOptionText2={props.sibappBtnOptionText}
							isMobile={props.isMobile}
						/>
					</div>
				</div>
			</div>
		);
	} else {
		return (
			<div className="downloadPage_mainContainer_MobileView">
				<div className="downloadPage_background_MobileView" />
				<img
					className="downloadPage_imageRightBackgroundStyle_MobileView"
					src={props.rightImage}
					alt="Takhfifn"
				/>
				<img
					className="downloadPage_imageLeftBackgroundStyle_MobileView"
					src={props.leftImage}
					alt="Takhfifn"
				/>
				<div className="downloadPage_topContainer_MobileView">
					<img className="downloadPage_imageLogoStyle_MobileView" src={props.logoImage} alt="Takhfifn" />
					<p className="downloadPage_pTitleTextRow1_MobileView">{props.titleTextRow1}</p>
					<p className="downloadPage_pTitleTextRow2_MobileView">{props.titleTextRow2}</p>
				</div>
				<div className="downloadPage_bottomContainer_MobileView">
					<div className="downloadPage_downloadBtnsContainer_MobileView">
						<DownloadBtn
							btnTitle={'Download Android'}
							onBtnClick={props.onAndroidBtnClick}
							isExpand={props.isAndroidExpand}
							btnOptionLogo1={props.googleLogo}
							btnOptionText1={props.googleBtnOptionText}
							btnOptionLogo2={props.cafebazaarLogo}
							btnOptionText2={props.cafebazaarBtnOptionText}
							isMobile={props.isMobile}
						/>
						<DownloadBtn
							btnTitle={'Download iOS'}
							onBtnClick={props.onIosBtnClick}
							isExpand={props.isIosExpand}
							btnOptionLogo1={props.appleLogo}
							btnOptionText1={props.appleBtnOptionText}
							btnOptionLogo2={props.sibappLogo}
							btnOptionText2={props.sibappBtnOptionText}
							isMobile={props.isMobile}
						/>
					</div>
				</div>
			</div>
		);
	}
};

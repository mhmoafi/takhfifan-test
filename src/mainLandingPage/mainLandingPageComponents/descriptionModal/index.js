// @flow
import React from 'react';
import './descriptionModal.css';

type propsType = {
	descriptionTitleTextRow1: string,
	descriptionTitleTextRow2: string,
	descriptionText: string,
	onDownloadBtnClick: Function
};

export default (props: propsType) => {
	return (
		<div className="descriptionModal_mainContainer">
			<div className="descriptionModal_textContainer">
				<p className="descriptionModal_pTitleTextRow1">{props.descriptionTitleTextRow1}</p>
				<p className="descriptionModal_pTitleTextRow2">{props.descriptionTitleTextRow2}</p>
				<p className="descriptionModal_pDescriptionText">{props.descriptionText}</p>
			</div>
			<div className="descriptionModal_btnContainer" onClick={props.onDownloadBtnClick}>
				<p className="descriptionModal_pBtn">Download Aplication</p>
			</div>
		</div>
	);
};

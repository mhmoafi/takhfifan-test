// @flow
import React, { Component } from 'react';
import './mainLandingPage.css';
import DescriptionModal from './mainLandingPageComponents/descriptionModal';
import DownloadPage from './mainLandingPageComponents/downloadPage';
import logoImage from '../assets/images/logo@2x.png';
import rightImage from '../assets/images/category.png';
import leftImage from '../assets/images/search.png';
import appleLogo from '../assets/images/apple@2x.png';
import googleLogo from '../assets/images/google@2x.png';
import sibappLogo from '../assets/images/sibapp@2x.png';
import cafebazaarLogo from '../assets/images/caafebazar@2x.png';
import mainLandingPageLeftIphoneImage from '../assets/images/iphone@2x.png';
import mainLandingPageLeftNexusImage from '../assets/images/nexus@2x.png';

type propsType = {};

type stateType = {
	descriptionTitleTextRow1: string,
	descriptionTitleTextRow2: string,
	descriptionText: string,
	viewState: number,
	isIosExpand: boolean,
	isAndroidExpand: boolean,
	appleBtnOptionText: string,
	googleBtnOptionText: string,
	sibappBtnOptionText: string,
	cafebazaarBtnOptionText: string,
	isFirstLoad: boolean,
	isMobile: boolean
};

export default class MainLandingPage extends Component<propsType, stateType> {
	constructor(props: propsType) {
		super(props);
		const currentPathname = (typeof window !== 'undefined' && window.location.pathname) || '';
		this.state = {
			descriptionTitleTextRow1: 'Download',
			descriptionTitleTextRow2: 'Takhfifan App',
			descriptionText:
				"Takhfifan is the first Group buying website founded on 2010. Takhfifan is Offering 50-90% daily deals at restaurants, retailers and service providers. Takhfifan's application for both iOS and Android platforms",
			viewState: 0,
			isFirstLoad: true,
			isIosExpand: false,
			isAndroidExpand: false,
			appleBtnOptionText: 'App Store',
			googleBtnOptionText: 'Play Store',
			sibappBtnOptionText: 'Sib App',
			cafebazaarBtnOptionText: 'Bazaar',
			isMobile: false
		};
		this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
	}
	componentDidMount() {
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	updateWindowDimensions() {
		if (window.innerWidth <= 500) {
			this.setState({ isMobile: true });
		} else {
			this.setState({ isMobile: false });
		}
	}

	onDownloadBtnClick = () => {
		this.setState({ viewState: 1 });
	};
	onAndroidBtnClick = () => {
		this.setState({ isAndroidExpand: !this.state.isAndroidExpand, isFirstLoad: false });
	};
	onIosBtnClick = () => {
		this.setState({ isIosExpand: !this.state.isIosExpand });
	};

	viewHandler = () => {
		if (this.state.viewState == 0) {
			if (!this.state.isMobile) {
				return (
					<div className="mainLandingPage_descriptionModalContainer">
						<img
							className="mainLandingPage_imageIphoneBackground"
							src={mainLandingPageLeftIphoneImage}
							alt="Takhfifn"
						/>
						<img
							className="downloadPage_imageNexusBackground"
							src={mainLandingPageLeftNexusImage}
							alt="Takhfifn"
						/>
						<div className="mainLandingPage_rightBackgroundImageContainer">
							<img className="mainLandingPage_rightBackgroundImages" src={sibappLogo} alt="Takhfifn" />
							<img
								className="mainLandingPage_rightBackgroundImages"
								src={cafebazaarLogo}
								alt="Takhfifn"
							/>
							<img className="mainLandingPage_rightBackgroundImages" src={googleLogo} alt="Takhfifn" />
							<img className="mainLandingPage_rightBackgroundImages" src={appleLogo} alt="Takhfifn" />
						</div>
						<DescriptionModal
							descriptionTitleTextRow1={this.state.descriptionTitleTextRow1}
							descriptionTitleTextRow2={this.state.descriptionTitleTextRow2}
							descriptionText={this.state.descriptionText}
							onDownloadBtnClick={this.onDownloadBtnClick}
						/>
					</div>
				);
			} else {
				return (
					<div className="mainLandingPage_descriptionModalContainer_MobileView">
						<img
							className="mainLandingPage_imageIphoneBackground_MobileView"
							src={mainLandingPageLeftIphoneImage}
							alt="Takhfifn"
						/>
						<img
							className="downloadPage_imageNexusBackground_MobileView"
							src={mainLandingPageLeftNexusImage}
							alt="Takhfifn"
						/>
						<div className="mainLandingPage_rightBackgroundImageContainer_MobileView">
							<img
								className="mainLandingPage_rightBackgroundImages_MobileView"
								src={sibappLogo}
								alt="Takhfifn"
							/>
							<img
								className="mainLandingPage_rightBackgroundImages_MobileView"
								src={cafebazaarLogo}
								alt="Takhfifn"
							/>
							<img
								className="mainLandingPage_rightBackgroundImages_MobileView"
								src={googleLogo}
								alt="Takhfifn"
							/>
							<img
								className="mainLandingPage_rightBackgroundImages_MobileView"
								src={appleLogo}
								alt="Takhfifn"
							/>
						</div>
						<DescriptionModal
							descriptionTitleTextRow1={this.state.descriptionTitleTextRow1}
							descriptionTitleTextRow2={this.state.descriptionTitleTextRow2}
							descriptionText={this.state.descriptionText}
							onDownloadBtnClick={this.onDownloadBtnClick}
						/>
					</div>
				);
			}
		} else if (this.state.viewState == 1) {
			return (
				<DownloadPage
					rightImage={rightImage}
					leftImage={leftImage}
					logoImage={logoImage}
					titleTextRow1={this.state.descriptionTitleTextRow1}
					titleTextRow2={this.state.descriptionTitleTextRow2}
					onAndroidBtnClick={this.onAndroidBtnClick}
					onIosBtnClick={this.onIosBtnClick}
					isAndroidExpand={this.state.isAndroidExpand}
					isIosExpand={this.state.isIosExpand}
					appleLogo={appleLogo}
					googleLogo={googleLogo}
					sibappLogo={sibappLogo}
					cafebazaarLogo={cafebazaarLogo}
					appleBtnOptionText={this.state.appleBtnOptionText}
					googleBtnOptionText={this.state.googleBtnOptionText}
					sibappBtnOptionText={this.state.sibappBtnOptionText}
					cafebazaarBtnOptionText={this.state.cafebazaarBtnOptionText}
					isFirstLoad={this.state.isFirstLoad}
					isMobile={this.state.isMobile}
				/>
			);
		}
	};

	render() {
		//const { currentPathname } = this.state;
		return <div className="mainLandingPage_mainContainer">{this.viewHandler()}</div>;
	}
}
